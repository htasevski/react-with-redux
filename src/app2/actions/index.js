import _ from 'lodash'
import jsonPlaceHolder from '../apis/jsonPlaceHolder'


export const fetchPostsAndUser = () => async (dispatch,getState) => {
    await dispatch(fetchPosts()) // we wait for the API request to be completed before we move on and do anything else
    
    const userIds=_.uniq(_.map(getState().postReducer,'userId'))

    console.log(userIds)

    userIds.forEach(id => dispatch(fetchUser(id)))
}

export const fetchPosts = () =>{

    return async (dispatch)=>{
        const response = await jsonPlaceHolder.get('/posts')
        
        dispatch({type: 'FETCH_POSTS', payload : response.data})
        // we dont care about the other properties
        // we only take the response.data array !!! 
    }

}
// version1

export const fetchUser = (id) => {
    return async (dispatch)=>{
        const response = await jsonPlaceHolder.get(`/users/${id}`)

        dispatch({type: 'FETCH_USER', payload : response.data})
    }
}


//version2 - memoized 

// export const fetchUser = function (id)  {
//         return _.memoize(async function(dispatch){
//             const response = await jsonPlaceHolder.get(`/users/${id}`)
    
//             dispatch({type: 'FETCH_USER', payload : response.data})
//         })
//     }


// final memoized

    // export const fetchUser = id =>  dispatch =>{
    //     _fethUser(id,dispatch);
    // }

    // const _fethUser = _.memoize( async (id,dispatch) => {

    //     const response = await jsonPlaceHolder.get(`/users/${id}`)
    
    //         dispatch({type: 'FETCH_USER', payload : response.data})

    // } ) 