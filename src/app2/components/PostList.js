import React from 'react'
import UserHeader from './UserHeader'
import {connect} from 'react-redux'
import {fetchPostsAndUser} from '../actions'

class PostList extends React.Component{

    componentDidMount(){
        this.props.fetchPostsAndUser()
    }

    renderList(){
        return this.props.posts.map( post =>{
            return(
                <div className="item" key={post.id}>
                    <i className="large middle aligned icon user"></i>
                    <div className="content">
                        <div className="description">
                            <h2>{post.title}</h2>
                            <p>{post.body}</p>
                        </div>
                        <UserHeader userid={post.userId}/>
                    </div>
                </div>
            )
        })
    }

    render(){
        console.log(this.props.posts)
        return(
            <div className="ui relaxed divided list">{this.renderList()}</div>
        )
    }
}

const mapStateToProps = (state) =>{
    return{posts:state.postReducer}
}

export default connect(mapStateToProps,{fetchPostsAndUser})(PostList) ;