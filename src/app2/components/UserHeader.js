import React from 'react'
import {connect} from 'react-redux'
import {fetchPostsAndUser} from '../actions'

class UserHeader extends React.Component{
    
    // componentDidMount(){
    //     this.props.fetchUser(this.props.userid)

    // }
    render(){
        // const user = this.props.users.find((user)=>
        //     user.id ===this.props.userid
        // )
        const {user} = this.props;
        
        if(!user)
        return(
            <div>{null}</div>
        )
        return(
            <div className="header">{user.name}</div>
        )
    }
}
const mapStateToProps = (state, ownProps) =>{
    return {
        user : state.users.find((user)=> user.id ===ownProps.userid)
    }
}

// const mapStateToProps = (state) =>{
//     return{users:state.users}
// }


export default connect(mapStateToProps,{fetchPostsAndUser})(UserHeader)