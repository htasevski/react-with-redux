import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
// for app 2
import {createStore,applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
//
import App from './app2/components/App';
import reducers from './app2/reducers'


//const store1 =createStore(reducers)

const store2 = createStore(reducers,applyMiddleware(thunk)) 
ReactDOM.render(
    <Provider store={store2}>
        <App/>
    </Provider>, 
    document.getElementById('root'));

