import {combineReducers} from 'redux';

const songsReducer = () => {
    
    return [
        {
            title: '4 your eyes only ', duration: '8:30'
        },
        { title : 'Loco contigo', duration : '4:20'},
        { title :'Despasito', duration :'3:30'},
        { title : 'Jovano jovanke', duration :'2:49'}
    ];
}

const selectedSongReducer = (selectedSong = null, action ) => {
    if (action.type === 'SONG_SELECTED'){
        return action.payload;
    }
    return selectedSong;
}


export default combineReducers({
    songs: songsReducer,
    selectedSong: selectedSongReducer
})