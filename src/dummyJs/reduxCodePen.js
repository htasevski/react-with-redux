import Redux from 'redux'
console.clear()

// action creator 1

const createPolicy = (name, value) => {
  return {
    type:'CREATE_POLICY',
    payload:{
      name: name,
      ammount : value
    }
  };
}

// action creator 2

const deletePolicy = (name) => {
  return {
    type:'DELETE_POLICY',
    payload:{
      name: name,
    }
  };
}

// action creator 3

const createClaim = (name, value) => {
  return {
    type:'CREATE_CLAIM',
    payload:{
      name: name,
      ammount : value
    }
  };
}

// reducers 


const claimsHistory = (oldClaimsList =[], action) => {
  if(action.type ==='CREATE_CLAIM'){
    // proceed
    return[
      ...oldClaimsList, action.payload
    ];
  }
  
  // exit
  return oldClaimsList;
}

const accounting = (bagOfMoney =100,  action ) => {
  
    if(action.type === 'CREATE_CLAIM'){
        return bagOfMoney - action.payload.ammount
    }
    else if (action.type ==='CREATE_POLICY'){
      return bagOfMoney + action.payload.ammount
    }
    
    return bagOfMoney
  }

  const policies = (listOfPolicies = [], action) => {

    if(action.type === 'CREATE_POLICY'){
        return [...listOfPolicies,action.payload]
    }
    else if(action.type === 'DELETE_POLICY'){
         
            return (listOfPolicies.filter( name => 
                name !== action.payload.name
              ))
              
    }
    else return listOfPolicies;
  }
  const {createStore , combineReducers} = Redux;

const departments = combineReducers ({
  accounting : accounting,
  claimsHistory: claimsHistory,
  policies: policies
})

const store = createStore(departments);


const action = createPolicy('Alex',20);

store.dispatch(action);

store.dispatch(createPolicy('John',50))

store.dispatch(createClaim('Alex',100))

store.dispatch(deletePolicy('John'))

console.log(store.getState())


  
    
  